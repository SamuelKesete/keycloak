
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { initialize } from './Keycloak/keycloak';





const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <p>Please wait while Keycloak is loading</p>
)

initialize().then(() => {

  root.render(

    <React.StrictMode>
      <App />
    </React.StrictMode>
  );
}).catch(() => {
  <React.StrictMode>
    <h1>Keycloak failed to load. Please report to admin! ☹️</h1>
  </React.StrictMode>

})
reportWebVitals();
