
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import '.././../../src/App.css';
// import { Link, NavLink } from 'react-router-dom'
// import LogIn from '../../View/Login';
// import Profile from '../../View/Profile';
// import Product from '../../View/Product';
import keycloak from './../../Keycloak/keycloak';

import { Link } from 'react-router-dom';


const NavbarLayout = () => {

  // const navigate = useNavigate()


  return (
    <>

      <Navbar bg="dark" variant="dark" id='nav'>
        <Container id='container'>
          <Navbar.Brand to="/" id='logo'>KeyClock</Navbar.Brand>
          <Nav className="me-auto" id='navlink'>
            <Link to="/profile">Profile</Link>
            <Link to="/product">products </Link>
            <Link to="/dogs" >Dogs </Link>
            {
              keycloak.authenticated ?
              <Link to="/logout" 
                onClick={()=>{keycloak.logout()}}>LogOut</Link>
              :<Link to="/login" 
                onClick={()=>{keycloak.login()}}>Login</Link>

            }
            
            

          </Nav>
        </Container>
      </Navbar>

    </>
  )

}
export default NavbarLayout