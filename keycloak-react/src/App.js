
import { BrowserRouter, Route, Routes, NavLink } from 'react-router-dom';
import './App.css';
import NavbarLayout from './Components/Navbar/NavbarLayout'
import LogIn from './View/Login';
import Product from './View/Product';
import Profile from './View/Profile';
import Dogs from './View/Dogs';
import KeyClock from './View/KeyClock';

import TokenPage from './Components/TokenPage';






function App() {
  return (
    <BrowserRouter>
      <NavbarLayout />
      <div className="App">

        <Routes>
          <Route path="/" element={< KeyClock />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/login" element={<LogIn />} />
          <Route path="/Product" element={<Product />} />
          <Route path="/dogs" element={<Dogs />} />



          <Route path='/token' element={
            <TokenPage />

          } />

        </Routes>


      </div>
      <footer>

        <li><NavLink to="/login">Goto Token?</NavLink></li>
      </footer>

    </BrowserRouter>




  );
}

export default App;
